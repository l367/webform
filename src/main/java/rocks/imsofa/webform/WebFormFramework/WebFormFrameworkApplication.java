package rocks.imsofa.webform.WebFormFramework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFormFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFormFrameworkApplication.class, args);
	}

}
